document.querySelector('.liste__plus').addEventListener('click',function(){

    const req = new XMLHttpRequest();

    var total = document.querySelector('.liste__total').textContent;
    var offset = parseInt(total) + 1;

    /** @todo: completer $url par la bonne url de l'api permettant d'afficher plus de lignes dans la liste */
    var url = '';

    req.overrideMimeType('application/json');
    req.open('GET',url);
    req.onload = function(){
        const json = JSON.parse(req.responseText);
        
        json.results.forEach(ligne => {
            /** @todo: completer la boucle qui permet de créer un élément li et de l'ajouter à la liste */        
        });
        
        document.querySelector('.liste__total').textContent = parseInt(total) + json.results.length;


    }
    req.send();

});

function contrastFinder(hex){

    r = parseInt( hex.substr(0, 2), 16 );
    g = parseInt( hex.substr(2, 2), 16 );
    b = parseInt( hex.substr(4, 2), 16 );

    return (((r + g + b)/3) < 128 ? 'ffffff' : '000000' );

} 